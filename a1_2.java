/*
@author: Dewei Chen 
@date: 1-16-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a1_2.java
@description: this program acquires an integer in pounds from user
			  and converts it to ounces
*/

import java.util.Scanner;

public class a1_2 {
	public static void main(String[] args) {
		//Declare scanner class
		Scanner input = new Scanner(System.in);
		
		//Prompt user for pound value
		System.out.print("enter pounds: ");
		int pounds = input.nextInt();
		
		//Output the converted value
		System.out.print(pounds + " pounds is " + pounds * 16 + " ounces.");
	}
}
