/*
@author: Dewei Chen 
@date: 1-16-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a1_1.java
@description: this program outputs some texts in different formats.
*/

public class a1_1 {
	public static void main(String[] args) {
		//Print 4 different strings separated by a blank line
		System.out.println("Tran says this is her first computer program.\n");
		System.out.println("Tran says this is her\nfirst computer program.\n");
		System.out.println("Tran said, \"This is my first computer program.\"\n");
		System.out.println("Tran said, \"This is my\nfirst computer program.\"\n");
	}

}
